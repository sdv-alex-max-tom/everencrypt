##############################################
#                 IMPORTS                    #
##############################################
# import tkinter for our sweet GUI

from tkinter import *

# Import os for file IO
import os

# Pretty self-explanatory
from Crypto.Cipher import AES
from Crypto.Util.Padding import pad, unpad
from base64 import b64decode, b64encode

# So pass isn't logged in cleartext
import getpass

# For IV generation
import random

# For file selection
from tkinter import filedialog

# For hashing functions like idk maybe Hash() 
import hashlib

# Import for CSV parsing
import csv

# Import to check if file exist
from os.path import exists


##############################################
#                 FUNCTIONS                  #
##############################################

########## VAULT MANAGEMENT ##################

#Check if vault exists
def CheckVaultExists():
    return exists('vault.ee')

#Create a vault, prompting for a main password and storing its salted hash
def CreateVault(mainPassword):
    vault= open("vault.ee","w+")
    masterHash = (hashlib.sha256(("MASTER").encode('utf-8')).hexdigest())
    masterPassSalted = (hashlib.sha256(("MASTER").encode('utf-8')).hexdigest()) + (hashlib.sha256(mainPassword.encode('utf-8')).hexdigest())
    masterPassSaltedAndDoubleHashed = (hashlib.sha256(masterPassSalted.encode('utf-8')).hexdigest())
    vault.write("PassType,PassName,PassSalt,PassKey\n")
    vault.write("MasterPass,MASTER," + masterHash + "," + masterPassSaltedAndDoubleHashed +"\n" )

# Checks if the main password from user input is correct	
def CheckMainPass(mainPassword):
    with open('vault.ee', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if 'MASTER' == row['PassName']:
                masterPassSalted = \
                    hashlib.sha256('MASTER'.encode('utf-8'
                                   )).hexdigest() \
                    + hashlib.sha256(mainPassword.encode('utf-8'
                        )).hexdigest()
                masterPassSaltedAndDoubleHashed = \
                    hashlib.sha256(masterPassSalted.encode('utf-8'
                                   )).hexdigest()
                if masterPassSaltedAndDoubleHashed == row['PassKey']:
                    print("Key has been correctly validated, database can now be openeddd")
                    return True
                else:
                    print("something's fucked up bro")
                    return False
                            
# Adds an entry from user input, crypting the passKey with mainpassword        		
def VaultAddEntry(mainPassword, passType, passName, passKey):
    vault = open("vault.ee","a")
    passSalt = (hashlib.sha256(passName.encode('utf-8')).hexdigest())
    passKeyCrypted = encryptSring(passKey, mainPassword)   
    PKC_Bytes = str(passKeyCrypted).encode('ascii')
    base64_Bytes = b64encode(PKC_Bytes)
    passKeyCryptedEncoded = base64_Bytes.decode('ascii')
    vault.write(passType + "," + passName + "," + passSalt + "," + passKeyCryptedEncoded + "\n")
    vault.close()
    
# Removes entry from Vault
def VaultRemoveEntry():
    lines = list()
    group = input("What GROUP do you want to delete?")
    vault = open("vault.ee","r")
    reader = csv.reader(vault)
    for row in reader:
         lines.append(row)
         for field in row:
                if field == group:
                    lines.remove(row)
    with open('vault.ee', 'w') as writeFile:
        writer = csv.writer(writeFile)
        writer.writerows(lines)        

# Decrypts entry from vault
def VaultDecryptEntry(mainPassword, passName):
    with open('vault.ee', newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        for row in reader:
            if passName == row['PassName']:
                s = row['PassKey']
                base64_Bytes = s.encode('ascii')
                PKC_Bytes = b64decode(base64_Bytes)
                PKC = PKC_Bytes.decode('ascii')
                v = eval(PKC)
               	return decryptString(v, mainPassword)

        
########## FILE ENCRYPTION ###############

# Crypt file using a key and an input file
def Crypt(key,file_Encode):
    print(file_Encode)
    key = str(key).encode('UTF-8')
    key = pad(key, AES.block_size)
    with open(file_Encode, 'rb') as entry:
        data = entry.read()
        cipher = AES.new(key, AES.MODE_CFB)
        ciphertext = cipher.encrypt(pad(data, AES.block_size))
        iv = b64encode(cipher.iv).decode('UTF-8')
        ciphertext = b64encode(ciphertext).decode('UTF-8')
        to_write = iv + ciphertext
    entry.close()
    with open(file_Encode +'.everenc', 'w') as data:
        data.write(to_write)
    data.close()
    
# Derypt file using a key and an input file
def Decrypt(key,filedecode_name):
    key = str(key).encode('UTF-8')
    key = pad(key, AES.block_size)
    with open(filedecode_name, 'r') as entry:
        try:
            data = entry.read()
            length = len(data)
            iv = data[:24]
            iv = b64decode(iv)
            ciphertext = data[24:length]
            ciphertext = b64decode(ciphertext)
            cipher = AES.new(key, AES.MODE_CFB, iv)
            decrypted = cipher.decrypt(ciphertext)
            decrypted = unpad(decrypted, AES.block_size)
            x=filedecode_name
            x=x[:len(x)-8]
            with open(x, 'wb') as data:
                data.write(decrypted)
            data.close()
        except(ValueError, KeyError):
            print("Wrong password")

# Checks if group or key is provided, pass the right parameter to Decrypt(), Checks the hashes for integrity
def DecryptWrapper():
    k = key.get()
    g = group.get()
    fileName=upload.get()
    k = key.get()
    fileName=upload.get()
    if g == '':
          if  k == '':
             print("Something went wrong, you need to either provide a registered group or a manual key")
          else:
             print("Encrypting with one time key, gogo crypto!")
             Decrypt(k,fileName)
    else:
          hashToCompare = hash.get()
          hashConcat = (hashlib.sha256(g.encode('utf-8')).hexdigest() + HashSHA256File(fileName))
          groupSignedFileHash = (hashlib.sha256(hashConcat.encode('utf-8')).hexdigest())
          print("GroupSignedFile = " + groupSignedFileHash)
          if hashToCompare == groupSignedFileHash:
             print("Hashes match, file is clean and authentic!")
             k = VaultDecryptEntry(mainPassword, g)
             Decrypt(k,fileName)
          else:
             print("DATA CORRUPTION OR EAVESDROPPING/MITM")
    
# Checks if group or key is provided, pass the right parameter to Crypt() , Checks the hashes for integrity       
def EncryptWrapper():    
    k = key.get()
    g = group.get()
    fileName=upload.get()
    if g == '':
       if  k == '':
          print("Something went wrong, you need to either provide a registered group or a manual key")
       else:
          print("Encrypting with one time key, gogo crypto!")
          Crypt(k,fileName)
    else:
       k = VaultDecryptEntry(mainPassword, g)
       Crypt(k,fileName)
       hashConcat = (hashlib.sha256(g.encode('utf-8')).hexdigest() + HashSHA256File(fileName + ".everenc"))
       groupSignedFileHash = (hashlib.sha256(hashConcat.encode('utf-8')).hexdigest())
       print("GroupSignedFile = " + groupSignedFileHash)
       hash.set(groupSignedFileHash)
       hint.set("Send me!")
       
       
# Function to do the file hashing
def HashSHA256File(file):
   BLOCK_SIZE = 65536 
   file_hash = hashlib.sha256() 
   with open(file, 'rb') as f:
       fb = f.read(BLOCK_SIZE) 
       while len(fb) > 0: 
           file_hash.update(fb) 
           fb = f.read(BLOCK_SIZE) 
   return file_hash.hexdigest()
   
####### STRING ENCRYPTION #####

# Specific imports here

from Cryptodome.Cipher import AES
from Cryptodome.Random import get_random_bytes

def encryptSring(plain_text, password):
    # generate a random salt
    salt = get_random_bytes(AES.block_size)

    # use the Scrypt KDF to get a private key from the password
    privateKey = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)

    # create cipher config
    cipherConfig = AES.new(privateKey, AES.MODE_GCM)

    # return a dictionary with the encrypted text
    cipherText, tag = cipherConfig.encrypt_and_digest(bytes(plain_text, 'utf-8'))
    return {
        'cipherText': b64encode(cipherText).decode('utf-8'),
        'salt': b64encode(salt).decode('utf-8'),
        'nonce': b64encode(cipherConfig.nonce).decode('utf-8'),
        'tag': b64encode(tag).decode('utf-8')
    }

def decryptString(enc_dict, password):
    # decode the dictionary entries from base64
    salt = b64decode(enc_dict['salt'])
    cipherText = b64decode(enc_dict['cipherText'])
    nonce = b64decode(enc_dict['nonce'])
    tag = b64decode(enc_dict['tag'])
    

    # generate the private key from the password and salt
    privateKey = hashlib.scrypt(
        password.encode(), salt=salt, n=2**14, r=8, p=1, dklen=32)

    # create the cipher config
    cipher = AES.new(privateKey, AES.MODE_GCM, nonce=nonce)

    # decrypt the cipher text
    decrypted = cipher.decrypt_and_verify(cipherText, tag)

    return decrypted
    
    
####### FILE BROWSING #########    
    
def browseFiles():
    fileName = filedialog.askopenfilename(initialdir="",
                                          title="Select a File",
                                          filetypes=(('all files', '.*'), ('text files', '.txt')
                                                     ))
    fileUploadName = fileName
    upload.set(fileUploadName)

########## QUIT ###############    
    
def qExit():
    root.destroy()

##############################################
#                 MAIN - INIT                #
##############################################

if CheckVaultExists() == False:
	passwd1 = getpass.getpass(prompt='Please type a password to secure your vault [WARNING, DO NOT LOSE IT]: ', stream=None)
	passwd2 = getpass.getpass(prompt='Please type the same password again: ', stream=None)
	if passwd1 == passwd2:
		CreateVault(passwd1)

else:
	passwd1 = getpass.getpass(prompt='Type your main password: ', stream=None)
	if CheckMainPass(passwd1):
		print("it works up to here!")
	else:
		print("Something's fucked up boss")
		
addDeleteEntry =  ''		
while addDeleteEntry != "NO":
	mainPassword=passwd1
	addDeleteEntry = input("Do you want to ADD, DELETE group keys or NO?  ")
	if addDeleteEntry == "ADD":
	   passTypeInput = input("Is it a GROUP or a PERSONNAL key?  ")
	   passNameInput = input("What is the name for this key?  ")
	   passKeyInput = getpass.getpass(prompt='Please type the password you will use for this group : ', stream=None)
	   VaultAddEntry(mainPassword, passTypeInput, passNameInput, passKeyInput)
	if addDeleteEntry == "DELETE":
	   VaultRemoveEntry()


			
##############################################
#         MAIN - GUI - GENERATION            #
##############################################
	
# creating root object
root = Tk()

# defining size of window
root.geometry("640x532")

# setting up the title of window
root.title("EVERENCRYPT v1.1.2")
root.configure(bg='white')

Tops = Frame(root, width=1100)
Tops.pack(side=TOP)

canvas = Canvas(root, width = 400, height = 200, bd=0, highlightthickness=0)      
canvas.pack()      
img = PhotoImage(file="images/EVERENCRYPT.PNG")      
canvas.create_image(20,100, anchor="w", image=img)    
canvas.configure(bg='white',bd=0)

f1 = Frame(root, width=500, height=400)
f1.pack(side=LEFT)
f1.configure(bg='white')
hint = StringVar()
group = StringVar()
rand = StringVar()
key = StringVar()
hash = StringVar()
upload = StringVar()
fileUploadName = StringVar()

btnUpload = Button(f1, padx=16, pady=8, bd=16,
                   fg="black", font=('arial', 16, 'bold'),
                   width=10, text="Upload file", bg="powder blue", anchor="w",
                   command=browseFiles).grid(row=0, column=0)
txtFile = Entry(f1, font=('arial', 16, 'bold'),
                textvariable=upload, bd=10, insertwidth=10,
                bg="powder blue", justify='left')
txtFile.grid(row=0, column=1)

lblKey = Label(f1, font=('arial', 16, 'bold'),
               text="KEY", bd=16, anchor="w",bg="white")
lblKey.grid(row=2, column=0)
txtkey = Entry(f1, font=('arial', 16, 'bold'),
               textvariable=key, bd=10, insertwidth=10,
               bg="powder blue", justify='left')
txtkey.grid(row=2, column=1)

lblGroup = Label(f1, font=('arial', 16, 'bold'),
               text="GROUP", bd=16, anchor="w",bg="white")
lblGroup.grid(row=3, column=0)
txtGroup = Entry(f1, font=('arial', 16, 'bold'),
               textvariable=group, bd=10, insertwidth=10,
               bg="powder blue", justify='left')
txtGroup.grid(row=3, column=1)

lblHash = Label(f1, font=('arial', 16, 'bold'),
               text="HASH", bd=16, anchor="w",bg="white")
lblHash.grid(row=4, column=0)
txtHash = Entry(f1, font=('arial', 16, 'bold'),
               textvariable=hash, bd=10, insertwidth=10,
               bg="powder blue", justify='left')
txtHash.grid(row=4, column=1)

lblHint = Label(f1, font=('arial', 16, 'bold'),
               textvariable=hint, bd=16, anchor="w",bg="white")
lblHint.grid(row=4, column=2)

# Show message button
btnEncrypt = Button(f1, padx=16, pady=8, bd=16, fg="black",
                  font=('arial', 16, 'bold'), width=10,
                  text="Encrypt", bg="green",
                  command=EncryptWrapper).grid(row=9, column=0)

btnDecrypt = Button(f1, padx=16, pady=8, bd=16, fg="black",
                  font=('arial', 16, 'bold'), width=10,
                  text="Decrypt", bg="green",
                  command=DecryptWrapper).grid(row=9, column=2)


# keeps window alive
root.mainloop()
