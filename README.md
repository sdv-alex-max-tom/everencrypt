 # Everencrypt

<img alt="Twitter Follow" src="https://img.shields.io/twitter/follow/twitter?style=social">
<img alt="Twitch Status" src="https://img.shields.io/twitch/status/andyonthewings?style=social">
<img alt="Codacy grade" src="https://img.shields.io/codacy/grade/36412496">
<img alt="GitHub release (latest by date)" src="https://img.shields.io/github/downloads/sdv-alex-max-tom/everencrypt/latest/total">
<img alt="PyPI - Python Version" src="https://img.shields.io/pypi/pyversions/Django">

# Sommaire

- ### 1. Management de la qualité
- ### 2. Technologies utilisées
- ### 3. Rôles et repartition des tâches
- ### 4. Ensemble des fonctionnalités
- ### 5. Procédure de mise à jour
- ### 6. Procédure de test
- ### 7. Flow
- ### 8. Organisation du Git
- ### 9. Ressources communes
- ### 10. Ready
- ### 11. Outils
- ### 11. Canaux de communications
- ### 12. Done
- ### 13. Todo
- ### 14. Retex

# 1.Management de la qualité

Projet réalisé par Alexandre, Maxime et Thomas dans le cadre de notre cours de management de la qualité. Mise en place de différents indicateurs, process, conventions.

![image](https://user-images.githubusercontent.com/69973378/169814608-35d81661-4efb-42b8-b85f-7cf4a3363892.png)

Everencrypt vous permet de chiffrer et déchiffrer ainsi que de hasher des fichiers et des dossiers dans le but d'assurer leur confidentialité et leur intégrité.

# 2.Technologies utilisées

Nous utilisons Python en version 3.9.9 et les modules suivants :

- tkinter 0.1.0 pour l'interface graphique
- getpass 1.7.4 pour la saisie sans retour des mots de passe
- random (built-in) pour les vecteurs d'initialisation AES
- pycryptodome 3.9.7 pour les fonctions cryptographiques
- hashlib (built-in) pour les fonctions de hashage

# 3.Rôles et repartition des tâches

- Alexandre : 
        - Lead Developer & Tests 
        - 06 XX XX XX XX
        - alex@everencrypt.sdv

- Maxime : 
        - Developer,DevOps & Tests
        - 06 XX XX XX XX
        - max@everencrypt.sdv

- Thomas : 
        - Project Manager & DevOps
        - 06 XX XX XX XX
        - tom@everencrypt.sdv

# 4.Ensemble des fonctionnalités

- Stocker des mots de passe de manière sécurisé
- Chiffrer un fichier
- Déchiffrer un fichier
- Hasher et saler 
- Vérification d'intégrité

# 5.Procédure de mise à jour

- Les developpeurs ne peuvent faire que des "merge request"
- Release effectuée seulement par une personne désignée
- Merge seulement après application de la procédure de test et application des corrections
- Sauvegarde de la version avant Merge
- Un cycle de mise à jour régulier
- Mise à jour de sécurité "As Soon As Possible"

# 6.Procédure de test

- Passage du code dans SonarQube
- Tests unitaires (fonctionnalité par fonctionnalité)
- Tests d'intégration (Interaction entre les fonctionnalités)
- Tests systèmes (Performances)
- Revue de code effectué par un autre developpeur

# 7.Flow

Usage: python3 ./everencrypt

```mermaid
graph TD
    A[Start] --> B(Does Vault Exists?)
    B -->|YES| D[Prompt & check for main password]  --> C(Prompt to add or delete entry)
    B -->|NO| E[Prompt for main password & vault creation]  --> C(Prompt to add or delete entry)
    C --> |ADD|Y[Add entry to Vault]
    C --> |REMOVE|Z[Remove entry from Vault]
    C --> F[Open GUI]
    F --> G[File encryption]
    F --> H[File decryption]
    G -->I[Is this for a group?]
    H -->L[Is this for a group?]
        I -->|YES| M[Grab salt & Decrypt password from vault]
        I -->|NO| N[Use provided password to crypt file]
        L -->|YES| J[Grab salt & Decrypt password from vault]
        L -->|NO| O[Use provided password to decrypt file]
    M --> P[Print salted hash of crypted file for integrity to share with recipient]
    J --> Q[Does provided hash match ours?]
    P --> R[Use decrypted pass to crypt file]
    Q --> |YES|S[Use decrypted pass to decrypt file]
    Q --> |NO|T[Trigger the tampering warning ]
```

# 8.Organisation du Git

- Dossier spécifique pour les images (./images)
- Racine pour le code (.)
- Plusieurs branches "main" et "dev"

# 9.Ressources communes

- Sauvegardes DropBox (compte dédié, backup manuelle avant chaque MERGE)
- Wiki privé (procédure pour se connecter, pour réaliser des back up, etc...)

# 10.Ready

- Convention de nommage : camelCase (dromadaire) pour les variables, CamelCase (chameau) pour les fonctions
- Préparation de la seconde phase de test unitaire pour everencrypt 1.0.8
- Commentaires, variables et fonctions en anglais

# 11.Outils

- Nous utilisons VSCode comme IDE
- Gitlab comme plateforme de developpement
- SonarQube comme outil d'audit de code

# 12.Canaux de communications

- Trello, canal dédié au projet
- Teams, canal dédié au projet
- Une daily organisée de 10h à 10H30

# 13.Done

- Première phase de tests unitaires (80% des tests passés avec succès)
- Fonctionnalités de chiffrement
- Fonctionnalités de hashage
- Création du Vault pour stocker les identifiants de groupe automatisé au premier lancement
- Ajout, récupération et suppression des entrées du Vault opérationnel
- Interface graphique opérationnelle
- intégration du vault pour le chiffrement des mots de passe du groupe
- Intégrer les mécanismes de validation par hash
- Deuxième phase de test unitaire (100% des tests passés avec succès) 
- Améliorer l'interface graphique
- Choisir une licence (Beerware)
- Maîtriser le versionning (1.1.x pour un changement mineur, 1.x.1 pour un changement majeur ou intégration d'une nouvelle fonctionnalité complète, x.1.1 pour une refonte complète)

# 14.Todo

- ~~Intégrer l'utilisation du Vault~~
- ~~Intégrer les mécanismes de validation par hash~~
- ~~Améliorer l'interface graphique~~
- Ajouter un argument pour activer le mode debug (actuellement activé par défault)
- Abandonner l'utilisation du fichier CSV comme "vault" 
- Migrer vers une base de donnée
- Détailler les modalités du processus d'amélioration continue (PDCA)
- ~~Choisir une Licence~~
- ~~Maîtriser le versionning~~
- Mettre en place un programme de Bug Bounty
- Traduire le Readme en anglais
- Mise en place de plusieurs indicateurs (Consommation de ressources, statitiques Gitlab (Issues, Download, Merge Request), User Feedback)
- Intégrer du chiffrement Post-Quantique

# 15.Retex

- Difficultés sur l'utilisation de SonarQube

Remerciements:

https://github.com/Ameskour/AES-tkinter-crypter-and-decrypter- (Inspiration générale)

https://github.com/Mohak2000/File-encryption-decryption-using-AES (GUI & fonctions cryptographiques basiques)
